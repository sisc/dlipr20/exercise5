#Correction
##Task 1 - Very well done
###1.a)
3/3 P
###1.b)
3/3 P
###1.c)
3/3 P
###2)
5/5 P
##Task 2
7/8 P - The code looks very correct, almost all steps were correctly perfomed, but activation maps do not fit to image. -> check code again.
#Total 21/22 P Still very good :)



**RWTH Aachen - Deep Learning in Physics Research SS2020**

**Exercise 5 Magnetic Phases**

Date: 23.05.2020

Students:
| student            | rwth ID |
|--------------------|---------|
| Baptiste Corneglio | 411835|
| Florian Stadtmann  | 367319 |
| Johannes Wasmer    |  090800 |


<!-- markdown-toc start - Don't edit this section. Run M-x markdown-toc-refresh-toc -->
**Table of Contents**

- [Task 1: Classification of magnetic phases](#task-1-classification-of-magnetic-phases)
    - [Testing different classification methods](#testing-different-classification-methods)
        - [I. Toy Model](#i-toy-model)
            - [Defining the Toy Model](#defining-the-toy-model)
            - ['Training' the Toy Model](#training-the-toy-model)
            - [Evaluationg the Toy Model](#evaluationg-the-toy-model)
        - [II. Fully Connected Network](#ii-fully-connected-network)
            - [Defining the Fully Connected Network](#defining-the-fully-connected-network)
            - [Training the Fully Connected Network](#training-the-fully-connected-network)
            - [Evaluating the Fully Connected Network](#evaluating-the-fully-connected-network)
        - [Convolutional Neural Network](#convolutional-neural-network)
            - [Defining the Convolutional Neural Network](#defining-the-convolutional-neural-network)
            - [Training the Convolutional Neural Network](#training-the-convolutional-neural-network)
            - [Evaluating the Convolutional Neural Network](#evaluating-the-convolutional-neural-network)
    - [Accuracy vs Temperature](#accuracy-vs-temperature)
        - [Toy Model](#toy-model)
        - [Fully Connected Network](#fully-connected-network)
        - [Convolutional Neural Network](#convolutional-neural-network-1)
- [Task 2: Discriminative localization](#task-2-discriminative-localization)
    - [Preparing and Calculation](#preparing-and-calculation)
    - [Plots](#plots)

<!-- markdown-toc end -->


# Task 1: Classification of magnetic phases #

## Testing different classification methods ##

### I. Toy Model ###

#### Defining the Toy Model ####

The toy model described in the lecture is implemented in python. First, the training data is reshaped from a 2D array with 32x32 values to an 1D array with 1024 values. 
```python
def toy_model(epsilon, X_raw ,y_input):
    x_input = np.reshape(X_raw,(X_raw.shape[0],X_raw.shape[1]*X_raw.shape[2]))
```

Then, the weights and biases are defined as they are in the input

```python
    N = x_input.shape[1]
    
    W_1 = np.ones((3,N))
    W_1[1,:] = -1
    W_1 = W_1/(N*(1+epsilon))
    b_1 = epsilon/(1+epsilon)*np.array([-1,-1,1])
    W_2 = np.ones((2,3))
    W_2[0,0] = 2
    W_2[1,0] = -2 
    W_2[1,1] = -2 
    W_2[0,2] = -1 
```

The theta function is choosen as the activation function.
```python
    def sigma(x):
        x[x>=0]=1
        x[x<0]=0
        return x
```
Finally, the output of the Toy Model is calcualted

```python
    y_output=np.zeros((x_input.shape[0],2))
    for i in range(x_input.shape[0]):
        y_output[i,...] = sigma(np.matmul(W_2,sigma((np.matmul(W_1,x_input[i,:]))+b_1)))
```

and  the percentage of wrong predicted events as well as the accuracy and the prediction are returned
  
    y_out_0=0
    y_out_1=0

    for i in range(y_output.shape[0]):
        if y_output[i,0] == y_input[i]:
            y_out_0 +=1
        if y_output[i,1] == y_input[i]:
            y_out_1 +=1

    return y_out_0/len(y_input),y_out_1/len(y_input), y_output[:,1]

#### 'Training' the Toy Model ####

The toy model only has one free parameter: epsilon. To fix the parameter, 100 values from 0 to 1 are tested.
In the lecture notes, it says that epsilon is larger than 0 and much smaller than one. However, the best result is achieved for epsilon = 0.74 as can be seen in the following plot. This value for epsilon yields a training accuracy of approximately 98.177%. For the evaluation of the Toy Model with the test data, this epilon is used.

![](https://www.comet.ml/api/image/notes/download?imageId=DrWuU6WNjacFKYggqvursgWrW&objectId=b71f7e18fa7444f1a65c8550606aa208)

#### Evaluationg the Toy Model ####
 
The model is evaluated on the test data for epsiolon = 0.74. This results in a test accuracy of 98.275%. 


### II. Fully Connected Network ###
#### Defining the Fully Connected Network ####

Again, the x data has to be reshaped from 32x32 to 1024 and this time the y data has to be changed to categorical.

```python
# X data shaped from 32x32 to 1024
x_train = np.reshape(X_train,(X_train.shape[0],X_train.shape[1]*X_train.shape[2]))
x_test = np.reshape(X_test,(X_test.shape[0],X_test.shape[1]*X_test.shape[2]))

# preparing y data to categorical
y_train=keras.utils.to_categorical(y_train)
y_test=keras.utils.to_categorical(y_test)
```

For the Fully Connected Network (FCN), three layers are choosen. The first one is a dense layer with ReLU activation and 64 nodes. It is followed by a dropout layer with 0.125 rate. One additional dense layer with ReLU and 32 layers follows before a dense layer with softmax and 2 nodes gives a output between zero and one. 
One could chose additional layers, but the additional parameters would make the training more difficult. 

```python
model = keras.models.Sequential(
        [
            keras.layers.Dense(64, activation = "relu"),
            keras.layers.Dropout(0.125),
            keras.layers.Dense(32, activation = "relu"),
            keras.layers.Dense(2, activation = "softmax")
        ]
    )
```

For the loss, the categroical crossentropy function is used, while the optimization is doenw ith the Adam optimizer.

```python
model.compile(
    optimizer = keras.optimizers.Adam(),loss='categorical_crossentropy',
     metrics=["accuracy"]
     )
```


#### Training the Fully Connected Network ####

The model is trained with a batch size of 32 for either 500 epochs or until early stopping with patience 1 is called, which happens after a few epochs. 10% of the data is used for validation, which means that 90% remains for training.

```python
model.fit(
    x_train, y_train,
    batch_size = 32,
    epochs = 500,
    verbose = 2,
    validation_split=0.1,
    callbacks = [keras.callbacks.EarlyStopping(
        monitor='val_loss', patience=1
        )]
    )
```

The training is finished after 3 epochs already. More epochs would cause overfitting, as tested earlier. The finished training gives a validation accuracy of 97.36%.

#### Evaluating the Fully Connected Network ####

The evaluation is done using the test data. An accuracy of 97.725% is achieved. The accuracy is worse than the one of the Toy Model. To enshure that this is not caused by bad training, a network with the same shape as the Toy Model was tested too (the experiments are archived to keep the experiment list clear). But this network achieved lower accuracies around only 96% with multiple trainings, so the FCN presented above is kept.The test accuracy vs the temperature can be seen in the following plot. Again, the accuracy is good exept for the data samples around the critical temperature, where it drops even lower than for the Toy Model.



### Convolutional Neural Network ###
#### Defining the Convolutional Neural Network ####

This time the x data has to be reshaped differently to 32x32x1 per event because the Convolutional Neural Network (CNN) requires the additional array dimension as the information that only one channel is used compared to for example 3 for RGB images. The y data is transformed to categorical as it was done for the FCN.

```python
# X data reshaped from 32x32 to 32x32x1
x_train=np.reshape(X_train,(X_train.shape[0],X_train.shape[1],X_train.shape[2],1))
x_test=np.reshape(X_test,(X_test.shape[0],X_test.shape[1],X_test.shape[2],1))

# preparing y data to categorical
y_train=keras.utils.to_categorical(y_train)
y_test=keras.utils.to_categorical(y_test)
```

The CNN is build with more layers. Two convolutional layers with kernel size 3x3 and 16 feature maps reduce the image size to 28x28. They are followed by a max pooling layer with size 2x2, leaving the data with size 14x14. Another two convolutional layers with the same kernel size and amount of feature maps are used, reduceing the data to 10x10, at which point global average pooling sets in. After that, the data is flatten and a dense layer with 2 nodes and softmax activation is applied for the classification. Larger CNNs made the network untrainable, while smaller ones reduced the accuracy.

 ```python
model = keras.models.Sequential(
        [
            keras.layers.Conv2D(16,(3,3), input_shape=(32,32,1)),
            keras.layers.Conv2D(16,(3,3)),
            keras.layers.MaxPooling2D((2,2)),
            keras.layers.Conv2D(16,(3,3)),
            keras.layers.Conv2D(16,(3,3)),
            keras.layers.GlobalAveragePooling2D(),
            keras.layers.Flatten(),
            keras.layers.Dense(2, activation = "softmax")
        ]
    )
```

The model is compiled in ther same way as the FCN was compiled:

```python
model.compile(
    optimizer = keras.optimizers.Adam(),loss='categorical_crossentropy',
    metrics=["accuracy"]
    )
```

#### Training the Convolutional Neural Network ####

The CNN is trained with the same settings as the FCN was trained.

```python
model.fit(
    x_train, y_train,
    batch_size = 32,
    epochs = 500,
    verbose = 2,
    validation_split=0.1,
    callbacks = [keras.callbacks.EarlyStopping(
        monitor='val_loss', patience=1
        )]
    )
```
The training is finished after 4 epochs with a validation accuracy of 98.14%. Again, longer training periods are tested, but they yield decreased accuracy due to overfitting.

#### Evaluating the Convolutional Neural Network ####

Evaluation the CNN on the test data yields a test accuracy of 98.4%. Therefore, the CNN peforms best. While the increase does not seem to be that big at the first view, one has to take into account that all models peform good exept for the temperatures around the critical temperature Tcrit  = 2.27K. The accuracy increase around that temperature is much larger, as explained in the following section



## Accuracy vs Temperature ##

For the Toy Model and both networks the accuracy is plotted vs. the temperature. The plots are shown below:

### Toy Model ###
![](https://www.comet.ml/api/image/notes/download?imageId=PQvLIATIgRs1NWjnPvzhLfkSb&objectId=b71f7e18fa7444f1a65c8550606aa208)

One can see that the accuracy is very high at approximately 100% exept for data samples around the critical temperature Tcrit = 2.27. 
The Toy Models accuracy drops a bit for 2.1K to approximately 96% and then down to around 75% for 2.2K and 2.3K. After that, it goes back up again to 100%.

### Fully Connected Network ###

![](https://www.comet.ml/api/image/notes/download?imageId=CC2ek7eO3PcXL5NzXorvnghvr&objectId=b71f7e18fa7444f1a65c8550606aa208)

The FCN shows a similar drop, but at a little higher temperatures and while the dropping range is broader from 2.1K to 2.6K, the significant drop only accures for 2.3K which is the temperature sample closest to the critical temperature. However, for the 2.3K the accuracy goes down to 52% which means that the network is almost guessing there. Therefore, the Toy Model has a better overall accuracy.

### Convolutional Neural Network ###

![](https://www.comet.ml/api/image/notes/download?imageId=uVVrP0qwSODnD1VJYJORoQ4Kx&objectId=b71f7e18fa7444f1a65c8550606aa208)

The CNN achieves higher accuracies than both, the Toy model and the FCN. While the drop goes from 2.1K to 2.3K, the accuracy only drops to 77% and the peak is a smaller than the one of the Toy model. Together with the best overall accuracy, it is justified to claim that the CNN performs best.  


# Task 2: Discriminative localization #

Task 2 is done in a separate file using the previously trained and saved CNN model.

## Preparing and Calculation ##

The model is loaded with 

```python
model = keras.models.load_model("CNN.h5")
```
and the weights are extracted with 

```python
weights=[]
for layer in model.layers:
        weights.append(layer.get_weights())
```

The first two rightly and wrongly classified images from the test data sample are taken.

```python
# making lists with indices of rightly and wrongly classified images
wrong_classified = []
right_classified = []
for j in range(prediction.shape[0]):
    if prediction[j,0]>=0.5 and y_test[j,0] == True:
        wrong_classified.append(j)
    elif prediction[j,0]<0.5 and y_test[j,0] == False:
        wrong_classified.append(j)
    elif prediction[j,0]>=0.5 and y_test[j,0] == False:
        right_classified.append(j)
    elif prediction[j,0]<0.5 and y_test[j,0] == True:
        right_classified.append(j)
```
```python
images=[
    x_test[wrong_classified[0],...],
    x_test[wrong_classified[1],...],
    x_test[right_classified[0],...],
    x_test[right_classified[1],...]
    ]
```

To get the feature maps, a second model is loaded that gives the output of the last convolutional layer before global averaging.

```python
model_temp = keras.Model(inputs=model.inputs, outputs=model.layers[4].output)
```

With some reshaping of images and feature maps, the feature maps are extracted


    # feeding only a single image into the model
    test_image = np.expand_dims(images[i],axis=0)
    feature_maps = model_temp.predict(test_image)
    # reshaping the feature maps from 10x10 to 32x32
    feature_maps = tf.compat.v1.image.resize_bilinear(feature_maps, (32,32))
    # reshaping the image from 1x32x32x1 to 32x32
    test_image=np.reshape(test_image,(32,32)) 
    
The class activation maps are calculated by multiplying the weights with the corresponding feature maps
   
    # calculating the class activation map
    class_activation_map = np.sum(feature_maps*weights[7][0][:,0], axis=3)
    class_activation_map = np.reshape(class_activation_map,(32,32))

## Plots##

Plotting the test-image array and the class-activation-map array yields the following plots for rightly classified images

![](https://www.comet.ml/api/image/notes/download?imageId=WhipKAmgQzGRhQz0WSIY2Yn0j&objectId=b71f7e18fa7444f1a65c8550606aa208)

![](https://www.comet.ml/api/image/notes/download?imageId=7xg8hvRfHCOPS4pWF1j0md1ta&objectId=b71f7e18fa7444f1a65c8550606aa208)

and the following ones for wrongly classified images

![](https://www.comet.ml/api/image/notes/download?imageId=Ja26ubr7Y8A62xqKZogvawUxJ&objectId=b71f7e18fa7444f1a65c8550606aa208)

![](https://www.comet.ml/api/image/notes/download?imageId=jiMvssmWrknqaccICRR2OxEiD&objectId=b71f7e18fa7444f1a65c8550606aa208)

One can see that the network focuses on large patches of the same spin(color) to do the classification.
Around 2.27K this strategy does not work that well any more as there are a lot of fine structures inside. Thus the decrease in accuracy around the critical temperature.





