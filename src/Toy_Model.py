﻿# =================================================================================================
# Importing 
# =================================================================================================


from comet_ml import Experiment
import dlipr
from tensorflow import keras
import matplotlib.pyplot as plt
import numpy as np
np.set_printoptions(threshold=np.inf)

# =================================================================================================
# Loading Comet
# =================================================================================================


# Set up YOUR experiment - login to comet, create new project (for new exercise)
# and copy the statet command
# or just change the name of the workspace, and the API (you can find it in the settings)
experiment = Experiment(api_key="<HIDDEN>",
                        project_name="exercise5", workspace="irratzo")

# =================================================================================================
# Loading Raw data
# =================================================================================================


# Load the Ising dataset
data = dlipr.ising.load_data()

# plot some examples
data.plot_examples(5, fname='examples.png')

# features: images of spin configurations
X_train = data.train_images
X_test = data.test_images

# classes: simulated temperatures
T = data.classes

# labels: class index of simulated temperature
# create binary training labels: T > Tc?
Tc = 2.27
y_train = T[data.train_labels] > Tc
y_test = T[data.test_labels] > Tc

# =================================================================================================
# Defining Print funcion for Comet
# =================================================================================================

def printcomet(string):
    # Prints string and loads it to comet
    print(string)
    experiment.log_text(string)

# =================================================================================================
# Defining Toy Model
# =================================================================================================

def toy_model(epsilon, X_raw ,y_input):
    x_input = np.reshape(X_raw,(X_raw.shape[0],X_raw.shape[1]*X_raw.shape[2]))
    
    N = x_input.shape[1]
    
    W_1 = np.ones((3,N))
    W_1[1,:] = -1
    W_1 = W_1/(N*(1+epsilon))
    b_1 = epsilon/(1+epsilon)*np.array([-1,-1,1])
    W_2 = np.ones((2,3))
    W_2[0,0] = 2
    W_2[1,0] = -2 
    W_2[1,1] = -2 
    W_2[0,2] = -1 
    
    def sigma(x):
        x[x>=0]=1
        x[x<0]=0
        return x
    
    y_output=np.zeros((x_input.shape[0],2))
    for i in range(x_input.shape[0]):
        y_output[i,...] = sigma(np.matmul(W_2,sigma((np.matmul(W_1,x_input[i,:]))+b_1)))

        
    y_out_0=0
    y_out_1=0

    for i in range(y_output.shape[0]):
        if y_output[i,0] == y_input[i]:
            y_out_0 +=1
        if y_output[i,1] == y_input[i]:
            y_out_1 +=1

    return y_out_0/len(y_input),y_out_1/len(y_input), y_output[:,1]

# =================================================================================================
# 'Training' Toy Model
# =================================================================================================

printcomet("'Training' Model:")


# Find best epsilon
epsilon = np.linspace(0,1,101)
wrong_predicted_train = np.zeros(len(epsilon))
accuracy_train = np.zeros(len(epsilon))

for j in range(len(epsilon)):
    wrong_predicted_train[j],accuracy_train[j], k = toy_model(epsilon[j],X_train,y_train) 





# Plotting 1-accuracy logarithmic
fig= plt.figure("1-accuracy vs epsilon")
plt.plot(epsilon,1-accuracy_train,label="1-training_accuracy")
plt.yscale("log")
plt.xlabel("epsilon")
plt.ylabel("percentage of wrong predicted events")
plt.legend()
fig.savefig('./percentage_wrong_predicted_vs_epsion_log_toy_model.png', bbox_inches='tight')
experiment.log_figure(figure=fig)
 
# printing training results
printcomet("")
printcomet(["best 'training' accuracy =", np.max(accuracy_train)])
printcomet(["for epsilon =", epsilon[np.argmax(accuracy_train)]])

# =================================================================================================
# Testing Toy Model
# =================================================================================================

printcomet("continuing with test for given epsilon")

# Test model
wrong_predicted_test,accuracy_test,predicted_test_array = toy_model(epsilon[np.argmax(accuracy_train)],X_test,y_test) 

printcomet("")
printcomet(["test accuracy = ", accuracy_test])
printcomet(["for epsilon =", epsilon[np.argmax(accuracy_train)]])

# Prepare data for plotting test accuracy vs temperature
T_right=np.zeros(len(T))
T_all=np.zeros(len(T))
for j in range(predicted_test_array.shape[0]):
    for i in range(len(T)):
        if T[data.test_labels[j]] == T[i]:
            if predicted_test_array[j]==y_test[j]:
                T_right[i] += 1
            T_all[i] += 1

# Plotting test accuracy vs temperature
fig = plt.figure("test accuracy vs temperature")
plt.plot(T,T_right/T_all)
plt.xlabel("Teperature in K")
plt.ylabel("test accuracy")
fig.savefig('./test_accuracy_vs_temperature_toy_model.png', bbox_inches='tight')
experiment.log_figure(figure=fig)