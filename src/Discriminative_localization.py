﻿from comet_ml import Experiment
import dlipr
import tensorflow as tf
from tensorflow import keras
import matplotlib.pyplot as plt
import numpy as np
np.set_printoptions(threshold=np.inf)
# =================================================================================================
# Loading Comet
# =================================================================================================


# Set up YOUR experiment - login to comet, create new project (for new exercise)
# and copy the statet command
# or just change the name of the workspace, and the API (you can find it in the settings)
experiment = Experiment(api_key="<HIDDEN>",
                        project_name="exercise5", workspace="irratzo")

# =================================================================================================
# Loading Raw data
# =================================================================================================


# Load the Ising dataset
data = dlipr.ising.load_data()

# plot some examples
data.plot_examples(5, fname='examples.png')

# features: images of spin configurations
X_train = data.train_images
X_test = data.test_images

# classes: simulated temperatures
T = data.classes

# labels: class index of simulated temperature
# create binary training labels: T > Tc?
Tc = 2.27
y_train = T[data.train_labels] > Tc
y_test = T[data.test_labels] > Tc

# =================================================================================================
# Defining Print funcion for Comet
# =================================================================================================

def printcomet(string):
    print(string)
    experiment.log_text(string)


# =================================================================================================
# Preparing Data
# =================================================================================================

# X data reshaped from 32x32 to 32x32x1
x_test=np.reshape(X_test,(X_test.shape[0],X_test.shape[1],X_test.shape[2],1))

# preparing y data to categorical
y_test=keras.utils.to_categorical(y_test)

# =================================================================================================
# Loading Model
# =================================================================================================

model = keras.models.load_model("CNN.h5")

# =================================================================================================
# Evaluating Test Data
# =================================================================================================

# getting the model output for the test data
prediction = model.predict(x_test,batch_size=32)

# making lists with indices of rightly and wrongly classified images
wrong_classified = []
right_classified = []
for j in range(prediction.shape[0]):
    if prediction[j,0]<0.5 and y_test[j,0] == True:
        wrong_classified.append(j)
    elif prediction[j,0]>=0.5 and y_test[j,0] == False:
        wrong_classified.append(j)
    elif prediction[j,0]<0.5 and y_test[j,0] == False:
        right_classified.append(j)
    elif prediction[j,0]>=0.5 and y_test[j,0] == True:
        right_classified.append(j)

# =================================================================================================
# Loading Weights
# =================================================================================================

weights=[]
for layer in model.layers:
        weights.append(layer.get_weights())

# =================================================================================================
# Loading Feature Maps and generating Class Activation Map
# =================================================================================================

# create a model that extracts the feature maps before global averaging
model_temp = keras.Model(inputs=model.inputs, outputs=model.layers[4].output)

print(len(wrong_classified))
print(len(right_classified))

# chosing the images of interest and creating name tags
images=[
    x_test[wrong_classified[0],...],
    x_test[wrong_classified[1],...],
    x_test[right_classified[0],...],
    x_test[right_classified[1],...]
    ]
nametags=[
    "wrongly_classified_1",
    "wrongly_classified_2",
    "rightly_classified_1",
    "rightly_classified_2"
    ]
nametags_2=[
    "wrongly classified image 1, T="+str(round(T[data.test_labels[wrong_classified[0]]],2))+"K",
    "wrongly classified image 2, T="+str(round(T[data.test_labels[wrong_classified[1]]],2))+"K",
    "rightly classified image 1, T="+str(round(T[data.test_labels[right_classified[0]]],2))+"K",
    "rightly classified image 2, T="+str(round(T[data.test_labels[right_classified[1]]],2))+"K"
    ]


# looping over the amount of images
for i in range(len(images)):

    # feeding only a single image into the model
    test_image = np.expand_dims(images[i],axis=0)
    feature_maps = model_temp.predict(test_image)
    # reshaping the feature maps from 10x10 to 32x32
    feature_maps = tf.compat.v1.image.resize_bilinear(feature_maps, (32,32))
    
    # calculating the class activation map
    class_activation_map = np.sum(feature_maps*weights[7][0][:,0], axis=3)
    class_activation_map = np.reshape(class_activation_map,(32,32))
    
    # reshaping the image from 1x32x32x1 to 32x32
    test_image=np.reshape(test_image,(32,32)) 
    
    # plotting the image and the class activation map
    plt.figure(nametags_2[i])
    fig, ax = plt.subplots(1,2)
    fig.suptitle(nametags_2[i])
    ax[0].imshow(test_image)
    ax[0].set_title("Image")
    ax[1].imshow(class_activation_map)
    ax[1].set_title("Class Activation Map")
    fig.savefig("./discriminative_localization/"+nametags[i]+".png")
    experiment.log_figure(figure=fig)
    







