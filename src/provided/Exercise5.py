from comet_ml import Experiment
import dlipr
from tensorflow import keras

# Set up YOUR experiment - login to comet, create new project (for new exercise)
# and copy the statet command
# or just change the name of the workspace, and the API (you can find it in the settings)
experiment = Experiment(api_key="EnterYourAPIKey",
                        project_name="exercise5", workspace="EnterGroupWorkspaceHere")


"""
Exercise 5:

Task 1: Classify the magnetic phases in terms of
- a fully connected layer (FCL)
- a convolutional neural network (CNN)
- the toy model (s. lecture slides)
Plot test accuracy vs. temperature for both networks and for the toy model.

Task 2: Discriminative localization
Pick out two correctly and two wrongly classified images from the CNN.
Look at Exercise 4, task 2 (visualize.py) to extract weights and feature maps from the trained model.
Calculate and plot the class activation maps and compare them with the images in order to see which regions lead to the class decision.

Hand in a printout of your commented code and plots.

If you are interested in the data generation look at MonteCarlo.py.
"""

# Load the Ising dataset
data = dlipr.ising.load_data()

# plot some examples
data.plot_examples(5, fname='examples.png')

# features: images of spin configurations
X_train = data.train_images
X_test = data.test_images

# classes: simulated temperatures
T = data.classes

# labels: class index of simulated temperature
# create binary training labels: T > Tc?
Tc = 2.27
y_train = T[data.train_labels] > Tc
y_test = T[data.test_labels] > Tc
