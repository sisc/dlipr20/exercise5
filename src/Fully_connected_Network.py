﻿from comet_ml import Experiment
import dlipr
from tensorflow import keras
import matplotlib.pyplot as plt
import numpy as np
np.set_printoptions(threshold=np.inf)

# =================================================================================================
# Loading Comet
# =================================================================================================


# Set up YOUR experiment - login to comet, create new project (for new exercise)
# and copy the statet command
# or just change the name of the workspace, and the API (you can find it in the settings)
experiment = Experiment(api_key="<HIDDEN>",
                        project_name="exercise5", workspace="irratzo")

# =================================================================================================
# Loading Raw data
# =================================================================================================


# Load the Ising dataset
data = dlipr.ising.load_data()

# plot some examples
data.plot_examples(5, fname='examples.png')

# features: images of spin configurations
X_train = data.train_images
X_test = data.test_images

# classes: simulated temperatures
T = data.classes

# labels: class index of simulated temperature
# create binary training labels: T > Tc?
Tc = 2.27
y_train = T[data.train_labels] > Tc
y_test = T[data.test_labels] > Tc

# =================================================================================================
# Defining Print funcion for Comet
# =================================================================================================

def printcomet(string):
    print(string)
    experiment.log_text(string)


# =================================================================================================
# Building Fully connected Network
# =================================================================================================

model = keras.models.Sequential(
    [
        keras.layers.Dense(64, activation = "relu"),
        keras.layers.Dropout(0.125),
        keras.layers.Dense(32, activation = "relu"),
        keras.layers.Dense(2, activation = "softmax")
    ]
)

model.compile(
    optimizer = keras.optimizers.Adam(),loss='categorical_crossentropy',
    metrics=["accuracy"]
    )

# =================================================================================================
# Preparing Data
# =================================================================================================

# X data shaped from 32x32 to 1024
x_train = np.reshape(X_train,(X_train.shape[0],X_train.shape[1]*X_train.shape[2]))
x_test = np.reshape(X_test,(X_test.shape[0],X_test.shape[1]*X_test.shape[2]))

# preparing y data to categorical
y_train=keras.utils.to_categorical(y_train)
y_test=keras.utils.to_categorical(y_test)

# =================================================================================================
# Training model
# =================================================================================================

model.fit(
    x_train, y_train,
    batch_size = 32,
    epochs = 500,
    verbose = 2,
    validation_split=0.1,
    callbacks = [keras.callbacks.EarlyStopping(
        monitor='val_loss', patience=1
        )]
    )

# =================================================================================================
# Evaluating model
# =================================================================================================

results = model.evaluate(x_test,y_test,batch_size=32)

# saving model
model.save("FCN.h5")

printcomet(["test accuracy = ",results[1]])

# preparing data for plotting test accuracy vs temperature
predicted_test_array = model.predict(x_test,batch_size=32)
T_right=np.zeros(len(T))
T_all=np.zeros(len(T))
for j in range(predicted_test_array.shape[0]):
    for i in range(len(T)):
        if T[data.test_labels[j]] == T[i]:
            if predicted_test_array[j,0]>=0.5 and y_test[j,0] == True:
                T_right[i] += 1
            elif predicted_test_array[j,0]<=0.5 and y_test[j,0] == False:
                T_right[i] += 1
            T_all[i] += 1

# preparing data for plotting test accuracy vs temperature
fig = plt.figure("test accuracy vs temperature")
plt.plot(T,T_right/T_all)
plt.xlabel("Teperature in K")
plt.ylabel("test accuracy")
fig.savefig('./test_accuracy_vs_temperature_FCN.png', bbox_inches='tight')
experiment.log_figure(figure=fig)